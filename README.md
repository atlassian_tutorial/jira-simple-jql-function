# Adding a JQL Function to Jira

This example shows an Atlassian Jira plugin that adds a simple JQL function to Jira. It 
allows a user to search for issues only in those projects that the user has visited. 

For a complete description of the example, along with the steps to create it, see the 
tutorial at: [Adding a JQL Function to Jira][1].

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/jiradev/jira-platform/guides/search/tutorial-adding-a-jql-function-to-jira